const mongoose = require("mongoose");
const express = require("express");
const bcrypt = require("bcryptjs");
const cors = require("cors");

const app = express();
const port = 4000;

const userRoute = require("./routes/userRoute.js");
const productRoute = require("./routes/productRoute.js");
const cartRoute = require("./routes/cartRoute.js");
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoute);
app.use("/product", productRoute);
app.use("/cart", cartRoute);

mongoose.connect("mongodb+srv://admin:admin@e-com.hmvkzo5.mongodb.net/e-com?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.once('open', () => console.log('Now connected to Edson-Mongo DB Atlas'));
app.listen(process.env.PORT || port, () => {console.log(`API is now online on port ${process.env.PORT || port}`)
});