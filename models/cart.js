const mongoose = require("mongoose");

const prodSchema = new mongoose.Schema({
	productId: {
				type: String,
				ref: "Product"
			},
	quantity: {
				type: Number,
				default: 1
			},
	subTotal: {
				type: Number
			}
})

const cartSchema = new mongoose.Schema({
	userId: String,
	products: [{prodSchema}],
	totalAmount: Number
})
const Cart = mongoose.model("Cart", cartSchema);
const Prod = mongoose.model("Prod", prodSchema);
module.exports = {Cart, Prod};