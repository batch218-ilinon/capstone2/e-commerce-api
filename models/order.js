const mongoose = require("mongoose");
const Product = require("../models/product.js");

const orderSchema = new mongoose.Schema({
	userId: String,
	email: String,
	products: [
		{
			productId: {
				type: mongoose.SchemaTypes.ObjectId,
				ref: "Product"
			},
			name: String,
			quantity: {
				type: Number,
				default: 1
			}
		}
	],
	totalAmount: Number,
	purchasedOn: {
		type: Date,
		default: () => Date.now()
	}
})

module.exports = mongoose.model("Order", orderSchema);