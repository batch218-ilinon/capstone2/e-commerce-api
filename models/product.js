const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Name is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	brand: {
		type: String,
		required: [true, "Brand is required"]
	},
	ram: {
		type: Number,
		required: [true, "RAM is required"]
	},
	imgSrc:{
		type: String,
		required: [true, "Image src is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	sellCount: {
		type: Number,
		default: 0
	},
	features:{
		type: String,
		required: [true, "Features is required"]
	},
	createdOn: {
		type: Date,
		default: () => Date.now()
	}
})

module.exports = mongoose.model("Product", productSchema);