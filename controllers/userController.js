const mongoose = require("mongoose")
const bcrypt = require("bcryptjs");
const auth = require("../auth.js")
const User = require("../models/user.js");
const Order = require("../models/order.js");
const Product = require("../models/product.js");


module.exports.registerUser = async(reqBody) => {
	let matchEmail = await User.findOne({email: reqBody.email});
	if(matchEmail == null){
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10),
		isAdmin: reqBody.isAdmin
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
	}
	else{
		let message = Promise.resolve('{"message": "Email is already registered"}');
		return message.then((value) => {return value})
	}
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false;
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)};
			}
			else{
				let message = Promise.resolve('{"message": "Incorrect password"}');
				return message.then((value) => {return value});
			}
		}
	})
}

module.exports.userDetails = (req) => {
	return User.findById(req.userId).then(result => {
		result.password = "";
		return result;
	})
}

module.exports.setAdmin = async(req) => {
	if(req.isAdmin){
		await User.findOneAndUpdate({email: req.email}, {
			isAdmin: true
		})
		let user = await User.findOne({email: req.email})
		if(user.isAdmin){
			return true
		}
		else{
			return false
		}
	}
	else{
		return false
	}
}

module.exports.createOrder = async(req) => {
	const user = await User.findById(req.userId)
	if(req.isAdmin){
		let message = Promise.resolve('ADMIN is not allowed to create order');
		return message.then((value) => {return value})
	}
	else{
		let orders = []
		let inActiveProducts = []
		for(let i = 0; i<req.reqBody.length; i++){
			let prodId = req.reqBody[i].productId;
			let productInfo = await Product.findById(prodId);
			if(productInfo.isActive){
				orders.push(req.reqBody[i])
			}
			else{
				inActiveProducts.push(productInfo._id) 
			}
		}
		if(orders.length == 0){
			let message = Promise.resolve('{"message": "Products not available"}');
			return message.then((value) => {return value})
		}
		else if(orders.length !== req.reqBody.length){
			let total = 0;
			for(let i = 0; i<orders.length; i++){
				let prodId = orders[i].productId;
				let productQuantity = orders[i].quantity;
				let productInfo = await Product.findById(prodId);
				await Product.findByIdAndUpdate(prodId, {
					sellCount: productInfo.sellCount + productQuantity})
				total = total + (productInfo.price * productQuantity);
				orders[i].name = productInfo.name
			}
			const newOrder = new Order({
				userId: req.userId,
				email: user.email,
				products: orders,
				totalAmount: total
			})
			return newOrder.save().then((order, error) => {
			if(error){
				return false;
			}
			else{
				let toStringInActiveProducts = inActiveProducts.toString()
				let message = Promise.resolve( toStringInActiveProducts + " not available\n" + order);
				return message.then((value) => {return value})
			}
			})
		}
		else{
			const user = await User.findById(req.userId)
			let total = 0;
			for(let i = 0; i<orders.length; i++){
				let prodId = orders[i].productId;
				let productQuantity = orders[i].quantity;
				let productInfo = await Product.findById(prodId);
				await Product.findByIdAndUpdate(prodId, {
					sellCount: productInfo.sellCount + productQuantity
				});
				total = total + (productInfo.price * productQuantity);
				orders[i].name = productInfo.name
			}
			const newOrder = new Order({
				userId: req.userId,
				email: user.email,
				products: orders,
				totalAmount: total
			})
			return newOrder.save().then((order, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
			})
		}
	}
}

module.exports.myOrders = async(req) => {
	if(req.isAdmin){
		let message = Promise.resolve('ADMIN is not allowed to create order');
		return message.then((value) => {return value})
	}
	else{
		let orders = await Order.find({userId: req.userId}).sort({purchasedOn: -1})
		if(orders.length == 0){
			let message = Promise.resolve('Order history empty');
			return message.then((value) => {return value})
		}
		return orders;
	}
}

module.exports.allOrder = async(isAdmin) => {
	if(isAdmin){
		let orders = await Order.find({}).sort({purchasedOn: -1})
		let user = await User.findById(orders.userId)
		if(orders.length == 0){
			let message = Promise.resolve('Order history empty');
			return message.then((value) => {return value})
		}
		return orders;
	}
	else{
		let message = Promise.resolve('User must be ADMIN to retrieve all orders');
		return message.then((value) => {return value})
	}

}

module.exports.changePassword = async(reqBody) => {
	console.log(reqBody.password)
	let user = await User.findById(reqBody.userId);
	console.log(user)
	const isPasswordCorrect = bcrypt.compareSync(reqBody.password, user.password);
	console.log(isPasswordCorrect)
	if(isPasswordCorrect){
		let newPass = bcrypt.hashSync(reqBody.newPassword, 10)
		await User.findByIdAndUpdate(reqBody.userId, {password: newPass} )
		return true
	}else{
		return false
	}
}