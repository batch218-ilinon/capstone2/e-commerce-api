const Product = require("../models/product.js");
const mongoose = require("mongoose")

module.exports.registerProduct = (reqData) => {
	if(reqData.isAdmin){
		let newProduct = new Product({
			name: reqData.reqBody.name,
			description: reqData.reqBody.description,
			price: reqData.reqBody.price,
			brand: reqData.reqBody.brand,
			ram: reqData.reqBody.ram,
			imgSrc: reqData.reqBody.imgSrc,
			features: reqData.reqBody.features,
			isActive: reqData.reqBody.isActive
		})
		return newProduct.save().then((user, error) => {
		if(error){
			return false;
		}
		else{
			return true
		}
	})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to register product');
		return message.then((value) => {return value})
	}
}

module.exports.retrieveActiveProducts = () => {
	return Product.find({isActive:true}).then(result => {return result})
}

module.exports.retrieveSingleProduct = (productId) => {
	return Product.findById(productId).then(result => {
		return result;
	})
}

module.exports.retrieveAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
}

module.exports.retrieveTopProducts = () => {
	return Product.find({isActive: true}).sort({sellCount: -1})
	.then(result => {
		return result;
	})
}

module.exports.retrieveNewestProducts = () => {
	return Product.find({isActive: true}).sort({createdOn: -1})
	.then(result => {
		return result;
	})
}

module.exports.retrieveLowestProducts = () => {
	return Product.find({isActive: true}).sort({price: 1})
	.then(result => {
		return result;
	})
}

module.exports.retrieveHighestProducts = () => {
	return Product.find({isActive: true}).sort({price: -1})
	.then(result => {
		return result;
	})
}


module.exports.updateProduct = (productId, reqData) => {
	if(reqData.isAdmin){
		console.log(reqData.productUpdate.ram)
		return Product.findByIdAndUpdate(productId, 
			{
				name: reqData.productUpdate.name,
				description: reqData.productUpdate.description,
				price: reqData.productUpdate.price,
				brand: reqData.productUpdate.brand,
				ram: reqData.productUpdate.ram,
				imgSrc: reqData.productUpdate.imgSrc,
				features: reqData.productUpdate.features,
				isActive: reqData.productUpdate.isActive	
			}
		).then((updateProduct, error) => {
			if(error){
				return false
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to update product');
		return message.then((value) => {return value})
	}
}

module.exports.archiveProduct = (productId, isAdmin) => {
	if(isAdmin){
		return Product.findByIdAndUpdate(productId, 
			{
				isActive: false
			}
		).then((productId, error) => {
			if(error){
				return false
			}
			return Product.findById(productId)
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to archive product');
		return message.then((value) => {return value})
	}
}