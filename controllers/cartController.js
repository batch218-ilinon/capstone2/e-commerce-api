const mongoose = require("mongoose")
const Product = require("../models/product.js");
const {Cart, Prod} = require("../models/cart.js");
const User = require("../models/user.js");
const Order = require("../models/order.js");

module.exports.updateCart = async(req) => {
	const addToCartProduct = await Product.findById(req.prodId)
	if(req.isAdmin){
		let message = Promise.resolve('"message": "ADMIN is not allowed."');
		return message.then((value) => {return value})
	}
	else if(addToCartProduct.isActive == false){
		let message = Promise.resolve('"message": "Product is not available."');
		return message.then((value) => {return value})
	}
	else{
		let userCart = await Cart.findOne({userId: req.userId})
		const productInfo = await Product.findById(req.prodId)
		let prodIndex = 0;
		if(userCart != null){
			let check = false;
			let cartProd = {};
			for(let i = 0; i < userCart.products.length; i++){
				let prodInfo = await Prod.findById(userCart.products[i]._id)
				if(prodInfo.productId == req.prodId){
					check = true;
					cartProd = prodInfo
					break;
				}
				else{
					check = false;
				}
			}
			if(check){
				let totalQuantity = cartProd.quantity + req.quantity
				await Prod.findByIdAndUpdate(cartProd._id, {quantity: totalQuantity, subTotal: totalQuantity * productInfo.price})
				let total = 0;
				for(let i = 0; i < userCart.products.length; i++){
					let cartProducts = await Prod.findById(userCart.products[i]._id);
					total = total + cartProducts.subTotal;
				}
				await Cart.findByIdAndUpdate(userCart._id, {totalAmount: total})
				return true
			}
			else{
				const newProd = new Prod({
					productId: req.prodId,
					quantity: req.quantity,
					subTotal: productInfo.price * req.quantity
				})
				await newProd.save();
				await Cart.findByIdAndUpdate(userCart._id, {$push: {products: newProd}, totalAmount: userCart.totalAmount + productInfo.price * req.quantity})
				return true;
			}

		}
		else{
			let total = productInfo.price * req.quantity
			const newProd = new Prod({
				productId: req.prodId,
				quantity: req.quantity,
				subTotal: total
			})
			return newProd.save().then((prod, error) => {
				if(error){
					return false;
				}
				else{
					const newCart = new Cart({
						userId: req.userId,
						products:[newProd],
						totalAmount: total
					})
					return newCart.save().then((cart, error) => {
						if(error){
							return false;
						}
						else{
							return true;	
						}
					})	
				}
			})
		}	
	}
}

module.exports.updateCartProd = async(req) => {
	const cartProd = await Prod.findById(req.prodId)
	if(cartProd != null){
		const productInfo = await Product.findById(cartProd.productId)
		const subTotal = req.quantity * productInfo.price
		await Prod.findByIdAndUpdate(req.prodId, {quantity: req.quantity, subTotal: subTotal})
		let userCart = await Cart.findOne({userId: req.userId})
		let total = 0;
		for(let i = 0; i < userCart.products.length; i++){
			let cartProducts = await Prod.findById(userCart.products[i]._id);
			total = total + cartProducts.subTotal;
		}
		await Cart.findByIdAndUpdate(userCart._id, {totalAmount: total})
		return true
	}
	else{
		return false
	}
}

module.exports.removeProduct = async(req) => {
	if(req.isAdmin){
		let message = Promise.resolve('ADMIN is not allowed.');
		return message.then((value) => {return value})
	}
	else{
		let userCart = await Cart.findOne({userId: req.userId})
		if(userCart != null){
			let check = false;
			for(let i = 0; i < userCart.products.length; i++){
				let prodInfo = await Prod.findById(userCart.products[i]._id)
				if(prodInfo.productId == req.prodId){
					check = true;
					cartProd = prodInfo
					break;
				}
				else{
					check = false;
				}
			}
			if(check){
				await Prod.findByIdAndDelete(cartProd._id)
				await Cart.findByIdAndUpdate(userCart._id, {$pull: {products: {_id: cartProd._id}}})
				let updatedCart = await Cart.findById(userCart._id)
				let total = 0;
				for(let i = 0; i < updatedCart.products.length; i++){
					let cartProducts = await Prod.findById(updatedCart.products[i]._id);
						total = total + cartProducts.subTotal;
				}
				await Cart.findByIdAndUpdate(userCart._id, {totalAmount: total})
				return true
			}
			else{
				let message = Promise.resolve('{"message": "Product is already not included in cart."}');
				return message.then((value) => {return value})
			}
		}
		else{
			let message = Promise.resolve('{"message": "Product is already not included in cart".}');
			return message.then((value) => {return value})
		}
	}
}

module.exports.cartCheckout = async(req) => {
	console.log(req.userId)
	const user = await User.findById(req.userId)
	if(req.isAdmin){
		let message = Promise.resolve('ADMIN is not allowed.');
		return message.then((value) => {return value})
	}
	else{
		let userCart = await Cart.findOne({userId: req.userId})
		if(userCart != null){
			let cartProducts = []
			if(userCart.products.length > 0){
				for(let i = 0; i < userCart.products.length; i++){
					let prod = await Prod.findById(userCart.products[i]._id)
					let productInfo = await Product.findById(prod.productId)
					let prodInfo = { 
						productId: prod.productId,
						name: productInfo.name,
						quantity: prod.quantity
					}
					cartProducts.push(prodInfo)
					await Prod.findByIdAndDelete(prod._id)
					await Cart.findByIdAndUpdate(userCart._id, {$pull: {products: {_id: prod._id}}})
					await Product.findByIdAndUpdate(prod.productId, {
					sellCount: productInfo.sellCount + prod.quantity})
				}
				await Cart.findByIdAndUpdate(userCart._id, {totalAmount: 0})
				const newOrder = new Order({
					userId: req.userId,
					email: user.email,
					products: cartProducts,
					totalAmount: userCart.totalAmount
				})
				return newOrder.save().then((order, error) => {
				if(error){
					return false;
				}
				else{
					return true;
				}
				})
			}
			else{
				console.log("here1")
				let message = Promise.resolve('{"message": "Cart is empty."}');
				return message.then((value) => {return value})
			}
		}
		else{
			console.log("here2")
			let message = Promise.resolve('{"message": "Cart is empty."}');
			return message.then((value) => {return value})
		}
	}
}

module.exports.cartProducts = async(req) => {
	if(req.isAdmin){
		return false
	}
	else{
		let userCart = await Cart.findOne({userId: req.userId})
		if(userCart != null){
			let cartProducts = []
			if(userCart.products.length > 0){
				for(let i = 0; i < userCart.products.length; i++){
					let prod = await Prod.findById(userCart.products[i]._id)
					let productInfo = await Product.findById(prod.productId)
					let prodInfo = {
						prodId: prod._id,
						productId: productInfo._id,
						name: productInfo.name,
						description: productInfo.description,
						price: productInfo.price,
						brand: productInfo.brand,
						ram: productInfo.ram,
						imgSrc: productInfo.imgSrc,
						quantity: prod.quantity,
						subTotal: prod.subTotal
					}
					cartProducts.push(prodInfo)
				}
				let cart = {
					products: cartProducts,
					totalAmount: userCart.totalAmount
				}
				return cart;
			}
			else{
				return false
			}
		}
		else{
			return false
		}
	}
}