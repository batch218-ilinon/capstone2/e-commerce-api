const express = require("express");
const router = express.Router();
const auth = require("../auth.js");

const productController = require("../controllers/productController.js");

router.post("/register", (req, res) => {
	const reqData = {
		reqBody: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.registerProduct(reqData).then(resultFromController => res.send(resultFromController))
})

router.get("/active", (req, res) => {
	productController.retrieveActiveProducts().then(resultFromController => res.send(resultFromController))
})

router.get("/all", (req, res) => {
	productController.retrieveAllProducts().then(resultFromController => res.send(resultFromController))
})

router.get("/top", (req, res) => {
	productController.retrieveTopProducts().then(resultFromController => res.send(resultFromController))
})

router.get("/newest", (req, res) => {
	productController.retrieveNewestProducts().then(resultFromController => res.send(resultFromController))
})

router.get("/lowest", (req, res) => {
	productController.retrieveLowestProducts().then(resultFromController => res.send(resultFromController))
})

router.get("/highest", (req, res) => {
	productController.retrieveHighestProducts().then(resultFromController => res.send(resultFromController))
})

router.get("/:productId", (req, res) => {
	productController.retrieveSingleProduct(req.params.productId).then(resultFromController => res.send(resultFromController))
})

router.patch("/:productId/update", auth.verify, (req, res) => {
	const reqData = {
		productUpdate: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.updateProduct(req.params.productId, reqData).then(resultFromController => {
		res.send(resultFromController)
	})
})

router.patch("/:productId/archive", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin
	
	productController.archiveProduct(req.params.productId, isAdmin).then(resultFromController => {
		res.send(resultFromController)
	})
})

module.exports = router;