const express = require("express");
const router = express.Router();
const auth = require("../auth.js");


const userController = require("../controllers/userController.js");

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})

router.get("/details", (req, res) => {
	const reqData = {
		userId: auth.decode(req.headers.authorization).id
	}
	userController.userDetails(reqData).then(resultFromController => res.send(resultFromController))
})

router.post("/setAsAdmin", (req, res) => {
	const reqData = {
			isAdmin: auth.decode(req.headers.authorization).isAdmin,
			email: (req.body.email)
		}
	userController.setAdmin(reqData).then(resultFromController => res.send(resultFromController))
})

router.post("/checkout", auth.verify, (req, res) => {
	const reqData = {
		userId: auth.decode(req.headers.authorization).id,
		reqBody: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userController.createOrder(reqData).then(resultFromController => {
		res.send(resultFromController)
	})
})

router.get("/orders", (req, res) => {
	const IsAdmin = auth.decode(req.headers.authorization).isAdmin
	userController.allOrder(IsAdmin).then(resultFromController => res.send(resultFromController))
})

router.get("/myOrders", (req, res) => {
	const reqData = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId: auth.decode(req.headers.authorization).id
	}
	userController.myOrders(reqData).then(resultFromController => res.send(resultFromController))
})

router.post("/changePass", (req, res) => {
	const reqData = {
		userId: auth.decode(req.headers.authorization).id,
		password: req.body.password,
		newPassword: req.body.newPassword,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userController.changePassword(reqData).then(resultFromController => res.send(resultFromController))
})

module.exports = router;