const express = require("express");
const router = express.Router();
const auth = require("../auth.js");

const cartController = require("../controllers/cartController.js");

router.post("/checkout", auth.verify, (req, res) => {
	const reqData = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	cartController.cartCheckout(reqData).then(resultFromController => {
		res.send(resultFromController)
	})
})

router.post("/:productId", auth.verify, (req, res) => {
	const reqData = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		prodId: req.params.productId,
		quantity: req.body.quantity
	}
	cartController.updateCart(reqData).then(resultFromController => {
		res.send(resultFromController)
	})
})

router.post("/:prodId/update", auth.verify, (req, res) => {
	const reqData = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		prodId: req.params.prodId,
		quantity: req.body.quantity
	}
	cartController.updateCartProd(reqData).then(resultFromController => {
		res.send(resultFromController)
	})
})

router.post("/:productId/remove", auth.verify, (req, res) => {
	const reqData = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		prodId: req.params.productId
	}
	cartController.removeProduct(reqData).then(resultFromController => {
		res.send(resultFromController)
	})
})

router.get("/", auth.verify, (req, res) => {
	const reqData = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	cartController.cartProducts(reqData).then(resultFromController => {
		res.send(resultFromController)
	})
})

module.exports = router;